﻿using UnityEngine;

public class Fade : MonoBehaviour {

    private Sprite background;
    public bool fadeIn = true, doDestroy = true;
    private float value;
    public float fadeSpeed;
    private SpriteRenderer sr;

    // fade in is tamsaus i sviesu fade out atvirksciai

	void Start () {
        sr = GetComponent<SpriteRenderer>();
        value = fadeIn ? 1 : 0;
        if (!fadeIn) sr.color = new Color(1f, 1f, 1f, 0);
 	}
	
	void Update () {

        sr.color = new Color(1f, 1f, 1f, value);
        if (fadeIn && value > 0)
        {
            value -= fadeSpeed * Time.deltaTime;
        }
        else if (!fadeIn && value < 1)
        {
            value += fadeSpeed * Time.deltaTime;
        }
        else if(doDestroy) Destroy(gameObject);
	}

    public static Fade CreateFade(float speed, bool fIn, bool doD)
    {
        GameObject newObject = Instantiate(Resources.Load<GameObject>("Prefabs/Fader")) as GameObject;
        Fade yourObject = newObject.GetComponent<Fade>();
        yourObject.fadeSpeed = speed;
        yourObject.fadeIn = fIn;
        yourObject.doDestroy = doD;
        return yourObject;
    }
}
