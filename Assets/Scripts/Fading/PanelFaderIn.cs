﻿using UnityEngine;
using UnityEngine.UI;

public class PanelFaderIn : MonoBehaviour {

    private Image image;
    public float fadeSpeed = 0.01f;

    void Start()
    {
        image = GetComponent<Image>();
    }

    void Update()
    {
        if (image.color.a < 1)
        {
            image.color = new Color(image.color.r, image.color.g,
                image.color.b, image.color.a + fadeSpeed);
        }
        else Destroy(this);
    }
}
