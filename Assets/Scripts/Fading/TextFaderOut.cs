﻿using UnityEngine;
using UnityEngine.UI;
 
public class TextFaderOut : MonoBehaviour {

    private Text textField;
    public float fadeSpeed = 0.01f;

    void Start()
    {
        textField = GetComponent<Text>();
    }

    void Update()
    {
        if (textField.color.a > 0)
        {
            textField.color = new Color(textField.color.r, textField.color.g,
                textField.color.b, textField.color.a - fadeSpeed);
        } else Destroy(this);
    }
}
