﻿using System.Collections;
using UnityEngine;

public class SceneLoader : MonoBehaviour {

    private int askLevel = 0;
    private float fadeDelay = 0.8f;

    public void QuitApplication()
    {
        StartCoroutine("DelayerQuitCo");
    }
    public void loadLevel(int level)
    {
        askLevel = level;
        StartCoroutine("DelayerLLCo");
    }
    public void startSpeedrun(float C)
    {
        StaticVariables.Stage = 0;
        StaticVariables.CurrentButton = 1;
        StaticVariables.ElapsedTimer = 0;
        StaticVariables.ButtonCount = C;
        StaticVariables.Speedrun = true;
        StaticVariables.FinishRun = false;
        loadLevel(2);
    }

    private IEnumerator DelayerLLCo()
    {
        Fade.CreateFade(1.23f, false, false);
        yield return new WaitForSeconds(fadeDelay);
        StaticVariables.Stage++;
        Application.LoadLevel(askLevel);
    }
    private IEnumerator DelayerQuitCo()
    {
        Fade.CreateFade(1.3f, false, false);
        yield return new WaitForSeconds(fadeDelay);
        Application.Quit();
    }
}
