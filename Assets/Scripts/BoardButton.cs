﻿using UnityEngine;
using UnityEngine.UI;

public class BoardButton : MonoBehaviour {

    private float c = 0;
    public Text textField;
    private RectTransform rectTransform;
    public Vector2 offset;
    private Vector2 startMin, startMax;
    public bool rotateButton = false;
    public float rotateSpeed = 1;

	void Start () {
        rectTransform = GetComponent<RectTransform>();
        startMin = rectTransform.anchorMin;
        startMax = rectTransform.anchorMax;
        textField = GetComponentInChildren<Text>();
        rectTransform.anchorMin = new Vector2(startMin.x + offset.x, startMin.y + offset.y);
        rectTransform.anchorMax = new Vector2(startMax.x + offset.x, startMax.y + offset.y);
	}

	void Update () {
        RotationCheck();
	}

    public void ExamineClicked()
    {
        if (StaticVariables.CurrentButton.ToString() == textField.text)
        {
            StaticVariables.CurrentButton++; 
            rotateButton = true;
        }

        if (StaticVariables.CurrentButton - 1 == StaticVariables.ButtonCount)
        {
            if (StaticVariables.Speedrun) // speedrunas
            {
                if (StaticVariables.Stage == StaticVariables.SpeedrunStages) // baigesi
                {
                    GameObject.FindGameObjectWithTag("SceneLoader").
                    GetComponent<SceneLoader>().loadLevel(3);
                }
                else
                {
                    StaticVariables.CurrentButton = 1;
                    StaticVariables.ButtonCount++;
                    StaticVariables.Running = false;
                    GameObject.FindGameObjectWithTag("SceneLoader").
                        GetComponent<SceneLoader>().loadLevel(2);
                }
            }
            else // nespeedrunas
            {
                StaticVariables.FinishRun = true;
            }
        }
    }

    void RotationCheck()
    {
        if (rotateButton && rectTransform.localEulerAngles.y > 88 && rectTransform.localEulerAngles.y < 90)
            rectTransform.localEulerAngles = new Vector3(0, 90, 0);
        if (rotateButton && rectTransform.localEulerAngles.y < 90)
            rectTransform.localEulerAngles = new Vector3(0, rectTransform.localEulerAngles.y + rotateSpeed, 0);
    }

    public static GameObject Create(GameObject canvas, string txt, Vector2 pos)
    {
        GameObject newObject = Instantiate(Resources.Load<GameObject>("Prefabs/ButtonBoard")) as GameObject;
        BoardButton yourObject = newObject.GetComponent<BoardButton>();
        yourObject.transform.SetParent(canvas.transform, false);
        newObject.GetComponentInChildren<Text>().text = txt;
        yourObject.offset = pos;
        return newObject;
    }

}
