﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class PlayController : MonoBehaviour {

    public GameObject timeText, panel, titleText, canvas;
    public bool started = false, ended = false;
    private float elapsedTime;
    private List<Vector2> buttonPoints = new List<Vector2>();
    private List<GameObject> buttons = new List<GameObject>();

	void Start () {
        StaticVariables.Running = false;
        elapsedTime = 5.5f;
	}
	
	void Update () {
        //po pradzios
        if (StaticVariables.Running)
            StaticVariables.ElapsedTimer += Time.deltaTime;

        if (!StaticVariables.Running && elapsedTime <= 0)
        {
            foreach (GameObject g in buttons)
                g.GetComponentInChildren<TextFaderOut>().enabled = true;

            StaticVariables.Running = true;
            canvas.GetComponent<Canvas>().sortingOrder = 10;
            titleText.SetActive(false);
            timeText.GetComponent<TextFaderIn>().enabled = true;
            panel.GetComponent<PanelFaderOut>().enabled = true;
        }

        //pradzia
        StartCheck();
	}
    void StartCheck()
    {
        ///pradzia
        if ((Input.touchCount > 0 || Input.GetKeyDown(KeyCode.Space)) && !started)
        {

            started = true;
            for (int i = 1; i <= StaticVariables.ButtonCount; i++)
            {
                Vector2 v = GenerateButtonPoint();
                //Debug.Log(i + " " + v.ToString());
                buttons.Add(BoardButton.Create(canvas, i + "", v));
            }

            foreach (GameObject b in buttons)
                b.SetActive(true);
        }

        if (started && !StaticVariables.Running)
        {
            elapsedTime -= Time.deltaTime;
            string s = elapsedTime < 0.5f ? "Go!" : Mathf.Round(elapsedTime) + "";
            titleText.GetComponent<Text>().text = s;
        }
    }

    private Vector2 GenerateButtonPoint()
    {
        Vector2 v = buttonPoints.Count > 0 ? buttonPoints[0] :
            new Vector2(Random.Range(0, 8), -Random.Range(0, 6));

        foreach (Vector2 vv in buttonPoints)
        {
            while (v.x == vv.x && v.y == vv.y)
            {
                v = new Vector2(Random.Range(0, 8), -Random.Range(0, 6));
            }
        }

        buttonPoints.Add(v);
        return new Vector2((float)v.x / 10, (float)v.y / 10);;
    }
}
