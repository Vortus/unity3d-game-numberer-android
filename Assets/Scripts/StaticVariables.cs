﻿using UnityEngine;

public class StaticVariables : MonoBehaviour {

    public static int SpeedrunStages = 10;
    public static int Stage = 0; 
    public static bool Running = false;
    public static float CurrentButton = 1;
    public static float ElapsedTimer = 0;
    public static float ButtonCount = 20;
    public static bool Speedrun = false;
    public static bool FinishRun = false;

}
