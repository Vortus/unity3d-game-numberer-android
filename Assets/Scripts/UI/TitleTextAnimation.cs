﻿using UnityEngine;
using System.Collections;

public class TitleTextAnimation : MonoBehaviour {

    public float rotationSpeed, switchTime, scaleSpeed;
    private float dx = -1, tmpTimer; 

	void Start () {
        tmpTimer = Time.time;
	}
	
	void Update () {

        if (Time.time - tmpTimer > switchTime)
        {
            dx *= -1;
            tmpTimer = Time.time;
        }

        transform.localScale += new Vector3(dx * scaleSpeed, dx * scaleSpeed, 0);
        transform.Rotate(0, 0, dx * rotationSpeed * Time.deltaTime);

	}
}
