﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimerText : MonoBehaviour {

    private Text textField; 

	void Start () {
        textField = GetComponent<Text>();
	}
	
	void Update () {
        float mins = Mathf.RoundToInt(StaticVariables.ElapsedTimer) / 60;
        float secs = Mathf.Round(StaticVariables.ElapsedTimer) % 60;
        string s = mins > 0 ? (mins + "m " + secs + " s") : secs + " s";
        textField.text = "Stage " + StaticVariables.Stage + '\n' + s;
	}
}
